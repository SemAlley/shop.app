import {Component, OnInit, ElementRef} from "@angular/core";

import {Slide} from './../models/slide';

const SLIDER: Slide[] = [
    {
        id: 1,
        title: 'Aluminium Club',
        description: 'Experience Ray-Ban',
        imagePath: 'app/assets/images/banner_img.png',
        active: true
    },
    {
        id: 2,
        title: 'Some title',
        description: 'Extra slide description',
        imagePath: 'app/assets/images/banner_img.png'
    },
];
@Component({
    selector: 'my-home',
    templateUrl: 'app/view/home.component.html'
})

export class HomeComponent implements OnInit{
    public myInterval:number = 5000;
    public noWrapSlides:boolean = false;
    slider = SLIDER;

    ngOnInit(): void {
    }
}