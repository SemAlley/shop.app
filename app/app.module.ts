import {NgModule}      from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {routing} from "./app.routing";

/** Components imports */
import {AppComponent} from "./components/app.component";
import {HeaderComponent} from "./components/header.component";
import {HomeComponent} from "./components/home.component";

import { Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';

@NgModule({
    imports: [
        BrowserModule,
        routing,
        Ng2BootstrapModule
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        HomeComponent
    ],
    providers: [
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}