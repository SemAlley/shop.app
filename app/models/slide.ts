export class Slide {
    id: number;
    title: string;
    description: string;
    imagePath: string;
}