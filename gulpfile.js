var gulp = require('gulp');
var compass = require('gulp-compass');
var concat = require('gulp-concat');
var cmq = require('gulp-combine-media-queries');

gulp.task('compass', function () {
    gulp.src('./app/assets/src/*.scss')
        .pipe(compass({
            css: 'app/assets/css',
            sass: 'app/assets/sass'
        }))
        .on('error', function (err) {
            console.log(err)
        })
        .pipe(cmq({
            log: true
        }))
        .pipe(gulp.dest('/app/assets/css'));
});

gulp.task('js', function () {
    gulp.src(['src/app.js', 'src/**/*.js'])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./js'))
});

gulp.task('default', ['compass'], function () {
    gulp.watch('./app/assets/sass/**/*.scss', ['compass']);
});